Source: vagrant-librarian-puppet
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Gabriel Filion <gabster@lelutin.ca>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               librarian-puppet,
               puppet,
               vagrant
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/ruby-team/vagrant-librarian-puppet.git
Vcs-Browser: https://salsa.debian.org/ruby-team/vagrant-librarian-puppet
Homepage: https://github.com/mhahn/vagrant-librarian-puppet
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: vagrant-librarian-puppet
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: librarian-puppet,
         ruby | ruby-interpreter,
         puppet,
         vagrant,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Vagrant plugin to install Puppet modules using Librarian-Puppet
 vagrant-librarian-puppet is a Vagrant plugin that runs Librarian-Puppet
 before any provisioning step.
 .
 This makes sure that when you provision a guest machine with Puppet, the
 modules will be up to date and in the state you require them to be. This way,
 testing Puppet modules themselves or using Puppet modules to provision your
 guest machine will become easier.
